import express from 'express';
import {addPromoCoupon,getAllPromoCoupons} from '../controllers/promoCouponController.js'

const router = express.Router();

router.post('/',addPromoCoupon);
router.get('/',getAllPromoCoupons);

export default router;