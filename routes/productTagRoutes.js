import express from 'express';
import {addTag, updateTag, getTags, getTagById, deleteTag } from '../controllers/productTagController.js';

const router = express.Router();


router.post('/',addTag);
router.put('/update/:id',updateTag);
router.delete('/delete/:id',deleteTag);
router.get('/show/:id',getTagById);
router.get('/:tenantId/:companyId',getTags);

export default router;