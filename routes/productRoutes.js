import express from "express";
import {addProduct,updateProduct,genProductCode,getProducts,getProductsByCategory,getProductById,deleteProduct} from '../controllers/productController.js';
import validateToken from '../middleware/validateToken.js';

const router = express.Router();


router.post('/',validateToken,addProduct);
router.delete('/delete/:id',validateToken,deleteProduct);
router.put('/update/:id',validateToken,updateProduct);
router.get('/show/:id',validateToken,getProductById);
router.post("/search/:keyword",validateToken,getProducts);
router.get("/category/:categoryId",validateToken,getProductsByCategory);
router.post("/gencode/:cat",genProductCode);


export default router; 