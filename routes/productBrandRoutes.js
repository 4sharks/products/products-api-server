import express from 'express';
import {addBrand,updateBrand,getBrands,getBrandById,deleteBrand} from '../controllers/productBrandController.js'

const router = express.Router();

router.post('/',addBrand);
router.put('/update/:id',updateBrand);
router.delete('/delete/:id',deleteBrand);
router.get('/show/:id',getBrandById);
router.get('/:tenantId/:companyId',getBrands);

export default router;