import express from 'express';
import {getCategory,getCategoryById,addCategory,updateCategory,deleteCategory,getCategoryAndProducts} from '../controllers/productCategoryController.js';
import validateToken from '../middleware/validateToken.js';

const router = express.Router();

router.post('/',validateToken,addCategory);
router.get('/show/:id',validateToken,getCategoryById);
router.put('/update/:id',validateToken,updateCategory);
router.delete('/delete/:id',validateToken,deleteCategory);
router.get('/:tenantId/:companyId',validateToken,getCategory);
router.get('/products/:tenantId/:companyId',validateToken,getCategoryAndProducts);

export default router;