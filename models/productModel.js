import mongoose from 'mongoose';

const textWithLang = {
    text: String,
    lang: String
}

const productSchema = mongoose.Schema(
    {
        productCode:{
            type: String,
            require: [true,"Please add a product code"]
        },
        productBarcode:{
            type: String,
        },
        productFactoryCode:{
            type: String,
        },
        codeInStock:{
            type: String,
        },
        weightProduct:{
            type: String,
        },
        sizeProduct:{
            type: String,
        },
        productName:[textWithLang, {required:[true,"Please add a product Name"]}],
        productDesc:[textWithLang],
        productMetaKeywords:[textWithLang],
        productMetaDesc:[textWithLang],
        productTags:[String],
        productHomePic:String,
        productImages:[String],
        productType:{
            type: String,
            default: null,
        },
        productClass:{
            type: mongoose.Schema.Types.ObjectId,
            ref:'ProductClass',
            default: null,
        },
        productCategory:{
            type: [mongoose.Schema.Types.ObjectId],
            ref:'ProductCategory',
            default: null,
        },
        productBrand:{
            type: mongoose.Schema.Types.ObjectId,
            ref:'ProductBrand',
            default: null,
        },
        productWarhouse:{
            type: [mongoose.Schema.Types.ObjectId],
            ref:'ProductWarhouse',
            default: null,
        },
        uomTemplate:{
            type: Object
        },
        uomSale:{
            type: Object
        },
        uomPurchase:{
            type: Object
        },
        price:{
            type: Number,
        },
        cost:{
            type: Number,
        },
        qty:{
            type: Number,
        },
        isStock:{
            type: Boolean,
        },
        isShowOnWebsite:{
            type: Boolean,
        },
        isActive:{
            type: Boolean,
        },
        tenantId: {
            type: String,
            required:[true, 'Please add a tenant id']
        },
        companyId:{
            type: String,
            required:[true, 'Please add a company id']
        },
    },
    {
        timestamps: true,
    }
);

const productModel =  mongoose.model('Product',productSchema);


export default productModel;