import mongoose from "mongoose";

const textWithLang = {
    text: String,
    lang: String
}

const tagSchema = mongoose.Schema({
    tagName:[textWithLang, {required:[true,"Please add a product Name"]}],
    tagDesc:[textWithLang],
    isActive:{
        type: Boolean,
        default: true
    },
    tenantId: {
        type: String,
        required:[true, 'Please add a tenant id']
    },
    companyId:{
        type: String,
        required:[true, 'Please add a company id']
    }
},{
    timestamps: true,
});

const tagModel = mongoose.model('Tag',tagSchema);

export default tagModel;