import mongoose from "mongoose";

const textWithLang = {
    text: String,
    lang: String
}

const brandSchema = mongoose.Schema(
    {
        brandName:[textWithLang, {required:[true,"Please add a product Name"]}],
        brandDesc:[textWithLang],
        brandMetaKeywords:[textWithLang],
        isActive:{
            type: Boolean,
            default: true
        },
        isShowOnWebsite:{
            type: Boolean,
            default: true
        },
        tenantId: {
            type: String,
            required:[true, 'Please add a tenant id']
        },
        companyId:{
            type: String,
            required:[true, 'Please add a company id']
        }
    },
    {
        timestamps: true,
    });

    const brandModel = mongoose.model('ProductBrand', brandSchema);

    export default brandModel;