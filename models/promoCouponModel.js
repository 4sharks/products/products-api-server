import mongoose from "mongoose";

const textWithLang = {
    text: String,
    lang: String
}

const PromoCouponSchema = mongoose.Schema(
    {
        promoCode:{
            type: String,
            required: [true,'Promo Code is required'],
        },
        startDate:{
            type: Date,
            required:[true,'Start Date is required'],
        },
        expiredDate:{
            type: Date,
            required:[true,'Expired Date is required'],
        },
        promoName:[textWithLang],
        promoTypeAmount:{
            type: String,
            required: [true,'Promo Type is required'],
            default: 'Fixed'
        },
        promoAmount:{
            type: String,
            required: [true,'Promo Amount is required'],
        },
        promoClass:{
            type: String,
            default: 'NormalClass'
        },promoProductId:{
            type: [mongoose.Schema.Types.ObjectId],
            ref: 'Product',
            default: null
        },promoProductCategoryId:{
            type: [mongoose.Schema.Types.ObjectId],
            ref: 'ProductCategory',
            default: null
        },
        isActive:{
            type: Boolean,
            default: true
        },
        tenantId: {
            type: String,
            required:[true, 'Please add a tenant id']
        },
        companyId:{
            type: String,
            required:[true, 'Please add a company id']
        },
        branchId:{
            type: String,
            required:[true, 'Please add a branch id']
        }
    },{
        timestamps: true,
    }
);

const promoCouponModel = mongoose.model('PromoCoupon',PromoCouponSchema);

export default promoCouponModel;