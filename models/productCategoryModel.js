import mongoose from "mongoose";

const textWithLang = {
    text: String,
    lang: String
}

const productCategorySchema = mongoose.Schema(
    {
        categoryParent:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'ProductCategory'
            // type: String,
            // default:"main",
            // required:[true, 'Please select a categoryParent']
        },
        categoryClass:{
            type: String,
            default:"product",
            required:[true, 'Please select a categoryClass']
        },
        categoryCode: String,
        categoryName:[textWithLang, {required:[true,"Please add a category Name"]}],
        categoryDesc:[textWithLang],
        categoryMetaKeywords:[textWithLang],
        categoryMetaDesc:[textWithLang],
        isActive:{
            type: Boolean,
            default: true
        },
        isShowOnWebsite:{
            type: Boolean,
            default: true
        },
        tenantId: {
            type: String,
            required:[true, 'Please add a tenant id']
        },
        companyId:{
            type: String,
            required:[true, 'Please add a company id']
        },
    },
    {
        timestamps: true,
    }
)

const productCategoryModel = mongoose.model('ProductCategory',productCategorySchema);

export default productCategoryModel;