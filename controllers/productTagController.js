import tagModel from "../models/productTagModel.js";

const addTag = async (req,res) =>{
    const {
        tagName,
        tagDesc,
        isActive,
        tenantId,
        companyId
    } = req.body;

    if(tagName.length ===0){
        res.status(404);
        throw new Error(`Invalid Tag Name`);
    }

    const added = await tagModel.create({
        tagName,
        tagDesc,
        isActive,
        tenantId,
        companyId 
    });

    if(added){
        res.status(201).json({status:1 ,message:"Tag has been added successffuly",data:added});
    }else{
        res.status(404).json({"status":0,"message":"Error during add Tag"});
    }
}

const updateTag = async (req,res) =>{
    const {
        tagName,
        tagDesc,
        isActive,
        tenantId,
        companyId
    } = req.body;
    const id = req.params.id;

    if(!id || tagName.length ===0){
        res.status(404);
        throw new Error(`Invalid Tag Name`);
    }

    const updated = await tagModel.updateOne(
        {_id:id},
        {
            tagName,
            tagDesc,
            isActive,
            tenantId,
            companyId 
        }
    );


    if(updated){
        res.status(201).json({status:1 ,message:"Tag has been updated successffuly",data:updated});
    }else{
        res.status(404).json({"status":0,"message":"Error during update Tag"});
    }
}

const getTags = async(req,res) => {
    const {tenantId,companyId} = req.params;

    if(!tenantId && !companyId){
        res.status(404);
        throw new Error(`Invalid tenant or company`);
    }

    const tags = await tagModel.find({
        tenantId,companyId
    }).sort({createdAt: -1});

    if(tags){
        res.status(200).json(tags);
    }else{
        res.status(404).json({"status":0,"message":"No data found"});
    }
}

const getTagById = async(req, res) =>{
    const id = req.params.id;

    if(!id){
        res.status(404);
        throw new Error(`Invalid tenant or company`);
    }

    const tags = await tagModel.findOne({
        _id:id
    }).sort({createdAt: -1});

    if(tags){
        res.status(200).json(tags);
    }else{
        res.status(404).json({"status":0,"message":"No data found"});
    }

}

const deleteTag = async (req, res) => {
    const id = req.params.id;
    const deleted = await tagModel.deleteOne({_id:id});

    if (deleted) {
        return res.status(200).json({"status":1,"message":"Tag deleted successfully"});
    }else{
        return res.status(404).json({"status":0,"message":"Error during delete Tag"});
    }
}

export {addTag, updateTag, getTags, getTagById, deleteTag }

