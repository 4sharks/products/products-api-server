import asyncHandler from  'express-async-handler';
import Product from '../models/productModel.js';


//@desc Add a product
//@route POST /api/products/add
//@access public
const addProduct = asyncHandler(async (req,res) => {
    const {
        productCode,
        productBarcode,
        productFactoryCode,
        codeInStock,
        weightProduct,
        sizeProduct,
        productName,
        productDesc,
        productMetaKeywords,
        productMetaDesc,
        productTags,
        productHomePic,
        productImages,
        productType,
        productClass,
        productCategory,
        productBrand,
        productWarhouse,
        uomTemplate,
        uomSale,
        uomPurchase,
        price,
        cost,
        qty,
        isStock,
        isShowOnWebsite,
        isActive,
        tenantId,
        companyId
    } = req.body;
    if(!productName){
        res.status(400);
        throw new Error("Product name is required"); 
    }
    const keyword = productName[0]?.text?.toString()?.trim() || productName[1]?.text?.toString()?.trim()
    const normalizedKeyword = keyword.trim().replace(/\s+/g, ' ');
    const escapedKeyword = normalizedKeyword.replace(/[$^.*+?()[]\\{}|]/g, '\\$&'); // Escape special characters

    // const reg =  `/^\s*(${escapedKeyword})\s*$/`
    const reg = new RegExp(`^\s*(${escapedKeyword})\s*$`, 'i'); // 'i' for case-insensitive

    const products = await Product.find({
                "productName":{
                    $elemMatch: {
                        text : {$regex: reg },
                    }
                }    
    })

    if(products.length > 0){
        return res.status(201).json({"status":0,"message":"Product is already exists","data":products,"reg":reg});

    }


    // const productAvailable = await Product.find({
    //     "productName":{
    //         $elemMatch: productName[0]
    //     }
    // });

    // if(productAvailable.length > 0) {
    //     res.status(201).json({"status":0,"message":"Product is available",productAvailable});
    // }else{
        const product = await Product.create({
            productCode,
            productBarcode,
            productFactoryCode,
            codeInStock,
            weightProduct,
            sizeProduct,
            productName,
            productDesc,
            productMetaKeywords,
            productMetaDesc,
            productTags,
            productHomePic,
            productImages,
            productType,
            productClass,
            productCategory,
            productBrand,
            productWarhouse,
            price,
            uomTemplate,
            uomSale,
            uomPurchase,
            cost,
            qty,
            isStock,
            isShowOnWebsite,
            isActive,
            tenantId,
            companyId
        })
        if(product){
            return res.status(201).json({status:1 ,message:"Product has been added successffuly",data:product});
        }else{
            return res.status(404).json({"status":0,"message":"Error during add product"});
        }
   // }
});

const updateProduct = async(req, res) => {
    const id = req.params.id;
    const {
        productCode,
        productBarcode,
        productFactoryCode,
        codeInStock,
        weightProduct,
        sizeProduct,
        productName,
        productDesc,
        productMetaKeywords,
        productMetaDesc,
        productTags,
        productHomePic,
        productImages,
        productType,
        productClass,
        productCategory,
        productBrand,
        productWarhouse,
        uomTemplate,
        uomSale,
        uomPurchase,
        price,
        cost,
        qty,
        isStock,
        isShowOnWebsite,
        isActive,
        tenantId,
        companyId
    } = req.body;

    if(!productName){
        res.status(400).json({
            status:0 ,
            message:'Product name is required',
            data:[]
        });
        // throw new Error("Product name is required");
    }

    const updated = await Product.updateOne({_id:id},{
        productCode,
        productBarcode,
        productFactoryCode,
        codeInStock,
        weightProduct,
        sizeProduct,
        productName,
        productDesc,
        productMetaKeywords,
        productMetaDesc,
        productTags,
        productHomePic,
        productImages,
        productType,
        productClass,
        productCategory,
        productBrand,
        productWarhouse,
        price,
        uomTemplate,
        uomSale,
        uomPurchase,
        cost,
        qty,
        isStock,
        isShowOnWebsite,
        isActive,
        tenantId,
        companyId
    });
    if(updated){
        res.status(201).json({status:1 ,message:"Product has been updated successffuly",data:updated});
    }else{
        res.status(404).json({"status":0,"message":"Error during update product"});
    }
};

const getProductsByCategory = async (req,res) => {
    const categoryId = req.params.categoryId;
    const products = await Product.find({
        productCategory : categoryId
    }).populate('productBrand').populate('productCategory').sort({createdAt:-1});
    if(products){
        return res.status(200).json({
            status:1,
            message:'SUCCESS',
            data:products
        });
    }
    return res.status(404).json({
        status:0,
        message:'ERROR',
        data: products
    }); 
}

const genProductCode = async(req, res) => {
    const cat = req.params.cat;
    const {tenantId,companyId} = req.body;

    const product = await Product.findOne({
        productCode :{$regex : `^${cat}` },
        tenantId,
        companyId,
    }).sort({createdAt:-1})

    if(product){
        const _no = product?.productCode.toString()?.split('-')
        return res.status(200).json({
            status:1 ,
            message:'success product : '+product?.productCode,
            data: _no?.length > 0 ? _no[_no.length-1] : 0
        });
    }else{
        return res.status(200).json({
            status: 0,
            message: "No data found",
            data:0
        });
    }

}

const getProducts = asyncHandler(async(req, res) => {
    const keyword = req.params.keyword;
    const {tenantId,companyId,productType,numberInPage,numberPage} = req.body;
    
    // Pagination settings
    const pageSize = numberInPage || 200; // Number of documents per page
    const pageNumber = numberPage || 1; // Page number to retrieve
    
    // Calculate skip value based on page number and page size
    const skip = (pageNumber - 1) * pageSize;

    let products ;
    let countProducts;

    if(keyword === 'All'){
        if(productType){
            countProducts = await Product.countDocuments({
                "price": {
                $exists: true,
                $ne:null,
                },
                tenantId,
                companyId,
                productType:{
                    $in:[productType,'SalesAndPurchases']
                }
            });
            products = await Product.find({
                "price": {
                $exists: true,
                $ne:null,
                },
                tenantId,
                companyId,
                productType:{
                    $in:[productType,'SalesAndPurchases']
                }
            }).populate('productBrand').populate('productCategory').skip(skip).limit(pageSize).sort({createdAt:-1});
        }else{
            countProducts = await Product.countDocuments({
                "price": {
                $exists: true,
                $ne:null,
                },
                tenantId,
                companyId
            });
            products = await Product.find({
                "price": {
                $exists: true,
                $ne:null,
                },
                tenantId,
                companyId
            }).populate('productBrand').populate('productCategory').skip(skip).limit(pageSize).sort({createdAt:-1});
        }
    }else{
        countProducts = await Product.countDocuments({
            "productName":{
                $elemMatch: {
                    text : {$regex : keyword , $options: 'i'}
                }
            },
            tenantId,
            companyId,
            
        });
        products = await Product.find({
            "productName":{
                $elemMatch: {
                    text : {$regex : keyword , $options: 'i' }
                }
            },
            tenantId,
            companyId,
            
        }).populate('productBrand').populate('productCategory').skip(skip).limit(pageSize).sort({createdAt:-1});
    }
    if(products){
        return res.status(200).json({
            status:1 ,
            message:'success',
            data: products,
            paginate:{
                resultCount:countProducts,
                pageNumber: pageNumber,
                pageSize : pageSize
            }
        });
    }else{
        return res.status(404).json({
            status: 0,
            message: "No data found",
            data:[]
        });
    }
});

const getProductById = async (req, res) => {
    const id = req.params.id;
    const product = await Product.findOne({_id:id}).populate('productCategory').populate('productBrand').exec();
    if(product){
        return res.status(200).json({
            status:1 ,
            message:'success',
            data: product
        });
    }else{
        res.status(404).json({
            status: 0,
            message:"error getting product",
            data:[]
        });
    }
};

const deleteProduct = async(req,res) => {
    const id = req.params.id;
    const deleted = await Product.deleteOne({_id:id});
    if (deleted) {
        res.status(201).json({"status":1,"message":"Product deleted successfully","data":deleted});
    }else{
        res.status(404).json({"status":0,"message":"No data found"});
    }
}

export {addProduct,updateProduct,genProductCode,getProducts,getProductsByCategory,getProductById,deleteProduct};