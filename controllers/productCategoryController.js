import ProductCategory from "../models/productCategoryModel.js"; 
import Product from '../models/productModel.js';

const getCategory = async(req,res) => {
    const {tenantId,companyId} = req.params;    
    if(!tenantId && !companyId){
        res.status(404).json({
            status: 0 ,
            message:'Invalid tenant or company',
            data:[]
        });
    }

    const catogries = await ProductCategory.find({
        tenantId,companyId
    }).populate('categoryParent').sort({createdAt: -1});

    if(catogries){
        res.status(200).json({
            status: 1,
            message: 'Success',
            data: catogries
        });
    }else{
        res.status(404).json({
            status:0,
            message:"No data found",
            data:[]
        });
    }

}
const getCategoryAndProducts = async(req,res) => {
    const {tenantId,companyId} = req.params;    
    if(!tenantId && !companyId){
        res.status(404).json({
            status: 0 ,
            message:'Invalid tenant or company',
            data:[]
        });
    }

    const catogries = await ProductCategory.find({
        tenantId,
        companyId
    }).populate('categoryParent').sort({createdAt: -1});



    if(catogries){
        var _list = [];
        for (const element of catogries) {
            const products = await Product.find({
                productCategory: {$in:element._id},
                tenantId,
                companyId,
            }).sort({createdAt:-1});

            _list.push({
                _id:element._id,
                categoryName:element.categoryName,
                categoryParent:element.categoryParent,
                categoryDesc:element.categoryDesc,
                isActive:element.isActive,
                isShowOnWebsite:element.isShowOnWebsite,
                products: products})

        }

        res.status(200).json({
            status: 1,
            message: 'Success',
            data: _list
        });
    }else{
        res.status(404).json({
            status:0,
            message:"No data found",
            data:[]
        });
    }

}

const getCategoryById = async(req,res) => {
    const id = req.params.id;
    if(!id){
        res.status(404).json({
            status:0,
            message:"Invalid ID",
            data:[]
        });
        
    }

    const category = await ProductCategory.find({ _id:id }).populate('categoryParent');
    if(category){
        res.status(200).json({
            status: 1,
            message: 'Success',
            data: category
        });
    }else{
        res.status(404).json({
            status: 0,
            message: "No data found",
            data:[]
        });
    }

}
const addCategory = async(req,res) => {
    const {
        categoryParent,
        categoryClass,
        categoryCode,
        categoryName,
        categoryDesc,
        categoryMetaKeywords,
        categoryMetaDesc,
        isActive,
        isShowOnWebsite,
        tenantId,
        companyId
    } = req.body;

    if(categoryName.length ===0){
        res.status(404);
        throw new Error(`Invalid category`);
    }

    const added = await  ProductCategory.create({
        categoryParent,
        categoryClass,
        categoryCode,
        categoryName,
        categoryDesc,
        categoryMetaKeywords,
        categoryMetaDesc,
        isActive,
        isShowOnWebsite,
        tenantId,
        companyId
    });

    if(added){
        res.status(201).json({status:1 ,message:"Category has been added successffuly",data:added});
    }else{
        res.status(404).json({"status":0,"message":"Error during add Category"});
    }
}
const updateCategory = async(req,res) => {
    const {
        categoryParent,
        categoryClass,
        categoryName,
        categoryCode,
        categoryDesc,
        categoryMetaKeywords,
        categoryMetaDesc,
        isActive,
        isShowOnWebsite,
        tenantId,
        companyId
    } = req.body;

    const id = req.params.id;

    if( !id ){
        res.status(404);
        throw new Error(`Invalid category`);
    }

    const updated = await  ProductCategory.updateOne(
        {_id: id},
        {
            $set:{
                    categoryParent,
                    categoryClass,
                    categoryCode,
                    categoryName,
                    categoryDesc,
                    categoryMetaKeywords,
                    categoryMetaDesc,
                    isActive,
                    isShowOnWebsite,
                    tenantId,
                    companyId
                }
        });

    if(updated){
        res.status(201).json({status:1 ,message:"Category has been updated successffuly",data:updated});
    }else{
        res.status(404).json({"status":0,"message":"Error during update Category"});
    }
}
const deleteCategory = async(req,res) => {
    const id = req.params.id;
    const deleted = await ProductCategory.deleteOne({_id:id});
    if (deleted) {
        res.status(200).json({"status":1,"message":"Category deleted successfully"});
    }else{
        res.status(404).json({"status":0,"message":"Error during delete Category"});
    }
}

export {getCategory,getCategoryById,addCategory,updateCategory,deleteCategory,getCategoryAndProducts}