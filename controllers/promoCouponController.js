
import PromoCoupon from "../models/promoCouponModel.js";

export const addPromoCoupon = async(req,res) => {
    const {
        promoCode,
        startDate,
        expiredDate,
        promoName,
        promoTypeAmount,
        promoAmount,
        promoClass,
        promoProductId,
        promoProductCategoryId,
        isActive,
        tenantId,
        companyId,
        branchId
    } = req.body;

    if(!promoCode || !startDate || !expiredDate || !promoTypeAmount){
        res.status(400);
        throw new Error('All felids are required')
    }

    const checkAvaillable = await PromoCoupon.find({
        promoCode,
        startDate,
        expiredDate,
    });

    if(checkAvaillable.length > 0)return res.status(201).json({status:0,message:"PromoCoupon is available"});

    const Added = await PromoCoupon.create({
        promoCode,
        startDate,
        expiredDate,
        promoName,
        promoTypeAmount,
        promoAmount,
        promoClass,
        promoProductId,
        promoProductCategoryId,
        isActive,
        tenantId,
        companyId,
        branchId
    })

    if(Added){
        res.status(201).json({status:1,message:'Added successfully',data:Added})
    }
    res.status(404).json({status:0,message:"Error during add PromoCoupon"});
}

export const getAllPromoCoupons = async (req, res) =>{
    const promocoupons = await PromoCoupon.find({});
    if(!promocoupons) return res.status(404).json({status:0,message:"error while fetchin",data:promocoupons});
    res.status(200).json({status:1,message:"success",data:promocoupons});
}