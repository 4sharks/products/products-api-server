import brandModel from "../models/productBrandModel.js";

const addBrand = async (req,res) => {
    const {
        brandName,
        brandDesc,
        brandMetaKeywords,
        isActive,
        isShowOnWebsite,
        tenantId,
        companyId
    } = req.body;

    if(brandName.length ===0){
        res.status(404);
        throw new Error(`Invalid Brand Name`);
    }

    const added = await brandModel.create({
        brandName,
        brandDesc,
        brandMetaKeywords,
        isActive,
        isShowOnWebsite,
        tenantId,
        companyId 
    });

    if(added){
        res.status(201).json({status:1 ,message:"Brand has been added successffuly",data:added});
    }else{
        res.status(404).json({"status":0,"message":"Error during add Brand"});
    }

}

const updateBrand = async (req,res) => {
    const {
        brandName,
        brandDesc,
        brandMetaKeywords,
        isActive,
        isShowOnWebsite,
        tenantId,
        companyId
    } = req.body;
    const id = req.params.id;

    if(!id || brandName.length ===0){
        res.status(404);
        throw new Error(`Invalid Brand Name`);
    }

    const updated = await brandModel.updateOne(
        {_id:id},
        {
        brandName,
        brandDesc,
        brandMetaKeywords,
        isActive,
        isShowOnWebsite,
        tenantId,
        companyId 
    });

    if(updated){
        res.status(201).json({status:1 ,message:"Brand has been updated successffuly",data:updated});
    }else{
        res.status(404).json({"status":0,"message":"Error during update Brand"});
    }

}

const getBrands = async (req, res) => {
    const {tenantId,companyId} = req.params;

    if(!tenantId && !companyId){
        res.status(404).json({
            status: 0,
            message:'Invalid tenant or company',
            data:[]
        });
        
    }
    const brands = await brandModel.find({
        tenantId,companyId
    }).sort({createdAt: -1});

    if(brands){
        return res.status(200).json({
            status: 1,
            message:'Success',
            data:brands
        });
    }else{
        return res.status(404).json({
            status: 0,
            message:"No data found",
            data:[]
        });
    }
}

const getBrandById = async (req,res) => {
    const id = req.params.id;

    if(!id){
        return res.status(404).json({
            status: 0,
            message:"Invalid tenant or company",
            data:[]
        });
      
    }
    const brands = await brandModel.findOne({
        _id:id
    });

    if(brands){
        return res.status(200).json({
            status: 1,
            message:'Success',
            data:brands
        });
    }else{
        return res.status(404).json({
            status:0,
            message:"No data found",
            data:[]
        });
    }
}

const deleteBrand = async (req, res) => {
    const id = req.params.id;
    const deleted = await brandModel.deleteOne({_id:id});
    if (deleted) {
        res.status(200).json({"status":1,"message":"Brand deleted successfully"});
    }else{
        res.status(404).json({"status":0,"message":"Error during delete Brand"});
    }
}

export {addBrand,updateBrand,getBrands,getBrandById,deleteBrand}