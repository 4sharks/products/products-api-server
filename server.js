import  express from 'express';
import dotenv from 'dotenv' ;
import  cors from 'cors';
import cookieParser from 'cookie-parser';

dotenv.config();
import dbConnect from './config/dbConnection.js';
import productRoutes from './routes/productRoutes.js';
import promoCouponRoutes from './routes/promoCouponRoutes.js';
import productCategoryRoutes from './routes/productCategoryRoutes.js';
import productBrandRoutes from './routes/productBrandRoutes.js';
import productTagRoutes from './routes/productTagRoutes.js';


dbConnect();

const app = express();
const port = process.env.PORT || 3010;

app.use(cors({ origin: '*',}));
app.use(express.json());
app.use(cookieParser());

app.use('/products',productRoutes);
app.use('/products/promo-coupons',promoCouponRoutes);
app.use('/products/categories',productCategoryRoutes);
app.use('/products/brands',productBrandRoutes);
app.use('/products/tags',productTagRoutes);


app.listen(port,()=> {
    console.log(`Server listening on port ${port}`);
});